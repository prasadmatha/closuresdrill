function counterFactory(){
    return{
        increment(a){
            return a += 1
        },
        decrement(a){
            return a -= 1
        }
    }
}

module.exports = counterFactory
