const limitFunctionCallCount = require("../limitFunctionCallCount")

let n = 3

//callback functions
const message = function(){}//return undefined
const helloCallback = () => "hello";
const sum = (a,b) => a + b;
const diff = (a,b) => a - b;
const sumThree = (a,b,c) => a + b + c;


try{
    const invokecb = limitFunctionCallCount(helloCallback,n)
    console.log(invokecb())
    console.log(invokecb())
    console.log(invokecb())
    console.log(invokecb())
    console.log(invokecb())
}
catch(error){
    console.log(error)
}

