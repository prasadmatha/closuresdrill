const cacheFunction = require("../cacheFunction")

const helloCallBack = (name) => "hello " + name;

const message = (name) => "hello " + name;

const sumOfTwo = (a,b) => a + b;

const sumofThree = (a,b,c) => a + b + c;


try{
const result = cacheFunction(sumofThree)
console.log(result(1,2,3))
console.log(result(1,2,3))
console.log(result(1,2,3))
console.log(result(1,2,3))
console.log(result(1,2,6))
console.log(result(1,2,6))
}
catch(error){
    console.log(error)
}

